const express = require("express");
const connection = require("../database");

const router = express.Router();

router.get('/', (req, res) => {
    // Retrieve all posts from the database
        connection.query('SELECT * FROM notification', (err, results) => {
        if (err) {
            res.status(500).json({ error: 'Internal Server Error' });
        } else {
            res.status(201).json(results);
        }
    });
});

router.post('/', (req, res) => {
    const { Message, Subject, SenderID, RecipientID } = req.body;

    // Insert a new post into the database
    connection.query('INSERT INTO notification (Message, Subject, SenderID, RecipientID) VALUES (?, ?, ?, ?)', [Message, Subject, SenderID, RecipientID], (err, result) => {
        if (err) {
            res.status(500).json({ error: 'Internal Server Error' });
        } else {
            res.status(201).json({ message: 'notification created successfully' });
        }
    });
});


router.get('/:id', async (req, res) => {
    const id = req.params.id;
  
    // Insert the user into the database
    connection.query(
      'SELECT * FROM notification WHERE NotificationID = ?', [id],
      (err, results) => {
        if (err) {
          console.error('Error fetching notification:', err);
          res.status(500).json({ error: 'Error fetching notification' });
        }  
        
        if (results.length === 0) {
          res.status(404).json({ error: 'notification not found' });
        } else {
          const notification = results[0];
          res.status(201).json({ notification });
        }
      }
    );
  });

  router.patch('/update/:id', (req, res) => {
    const NotificationID = req.params.id;
    const { Message} = req.body;

    connection.query('UPDATE notification SET Message = ? WHERE NotificationID = ?', [Message, NotificationID], (err, result) => {
        if (err) {
            res.status(500).json({ error: 'Internal Server Error' });
        } else if (result.affectedRows === 0) {
            res.status(404).json({ error: 'notification not found' });
        } else {
            res.status(201).json({ message: 'notification updated successfully' });
        }
    });
});

router.delete('/delete/:id', (req, res) => {
    const NotificationID = req.params.id;

    connection.query('DELETE FROM notification WHERE NotificationID = ?', [NotificationID], (err, result) => {
        if (err) {
            res.status(500).json({ error: 'Internal Server Error' });
        } else if (result.affectedRows === 0) {
            res.status(404).json({ error: 'Notification not found' });
        } else {
            res.status(201).json({ message: 'Notification deleted successfully' });
        }
    });
});

module.exports = router;