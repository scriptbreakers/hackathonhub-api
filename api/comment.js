const express = require("express");
const connection = require("../database");

const router = express.Router();

router.get('/', (req, res) => {
    // Retrieve all posts from the database
        connection.query('SELECT * FROM comment', (err, results) => {
        if (err) {
            res.status(500).json({ error: 'Internal Server Error' });
        } else {
            res.status(201).json(results);
        }
    });
});

router.post('/', (req, res) => {
    const { Message, CreatedByID, CreatedByName, PostID, PostTitle } = req.body;

    // Insert a new post into the database
    connection.query('INSERT INTO comment (Message, CreatedByID, CreatedByName, PostID, PostTitle) VALUES (?, ?, ?, ?, ?)', [Message, CreatedByID, CreatedByName, PostID, PostTitle], (err, result) => {
        if (err) {
            res.status(500).json({ error: 'Internal Server Error' });
        } else {
            res.status(201).json({ message: 'Comment created successfully' });
        }
    });
});


router.get('/:id', async (req, res) => {
    const id = req.params.id;
  
    // Insert the user into the database
    connection.query(
      'SELECT * FROM comment WHERE PostID = ?', [id],
      (err, results) => {
        if (err) {
          console.error('Error fetching comment:', err);
          res.status(500).json({ error: 'Error fetching comment' });
        }  
        
        if (results.length === 0) {
          res.status(404).json({ error: 'comment not found' });
        } else {
          const comment = results[0];
          res.status(201).json({ comment });
        }
      }
    );
  });


  router.patch('/update/:id', (req, res) => {
    const CommentID = req.params.id;
    const { Message,} = req.body;

    connection.query('UPDATE comment SET Message = ? WHERE CommentID = ?', [Message, CommentID], (err, result) => {
        if (err) {
            res.status(500).json({ error: 'Internal Server Error' });
        } else if (result.affectedRows === 0) {
            res.status(404).json({ error: 'comment not found' });
        } else {
            res.status(201).json({ message: 'comment updated successfully' });
        }
    });
});


router.delete('/delete/:id', (req, res) => {
    const CommentID = req.params.id;

    connection.query('DELETE FROM comment WHERE CommentID = ?', [CommentID], (err, result) => {
        if (err) {
            res.status(500).json({ error: 'Internal Server Error' });
        } else if (result.affectedRows === 0) {
            res.status(404).json({ error: 'Comment not found' });
        } else {
            res.status(201).json({ message: 'Comment deleted successfully' });
        }
    });
});

module.exports = router;









