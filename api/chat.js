const express = require("express");
const connection = require("../database");

const router = express.Router();

router.get('/', (req, res) => {
    // Retrieve all chat from the database
        connection.query('SELECT * FROM chat', (err, results) => {
        if (err) {
            res.status(500).json({ error: 'Internal Server Error' });
        } else {
            res.status(201).json(results);
        }
    });
});
router.post('/', (req, res) => {
    const { Message, SenderID, SenderName, RecipientID, RecipientName } = req.body;

    // Insert a new post into the database
    connection.query('INSERT INTO chat (Message, SenderID, SenderName, RecipientID, RecipientName) VALUES (?, ?, ?, ?, ?)', [Message, SenderID, SenderName, RecipientID, RecipientName], (err, result) => {
        if (err) {
            res.status(500).json({ error: 'Internal Server Error' });
        } else {
            res.status(201).json({ message: 'chat created successfully' });
        }
    });
});


router.get('/:id', async (req, res) => {
    const id = req.params.id;
  
    
    connection.query(
      'SELECT * FROM chat WHERE ChatID = ?', [id],
      (err, results) => {
        if (err) {
          console.error('Error fetching chat:', err);
          res.status(500).json({ error: 'Error fetching chat' });
        }  
        
        if (results.length === 0) {
          res.status(404).json({ error: 'chat not found' });
        } else {
          const chat = results[0];
          res.status(201).json({ chat });
        }
      }
    );
  });


  router.patch('/update/:id', (req, res) => {
    const ChatID = req.params.id;
    const { Message,} = req.body;

    connection.query('UPDATE chat SET Message = ? WHERE ChatID = ?', [Message, ChatID], (err, result) => {
        if (err) {
            res.status(500).json({ error: 'Internal Server Error' });
        } else if (result.affectedRows === 0) {
            res.status(404).json({ error: 'chat not found' });
        } else {
            res.status(201).json({ message: 'chat updated successfully' });
        }
    });
});


router.delete('/delete/:id', (req, res) => {
    const ChatID = req.params.id;

    connection.query('DELETE FROM chat WHERE ChatID = ?', [ChatID], (err, result) => {
        if (err) {
            res.status(500).json({ error: 'Internal Server Error' });
        } else if (result.affectedRows === 0) {
            res.status(404).json({ error: 'Chat not found' });
        } else {
            res.status(201).json({ message: 'Chat deleted successfully' });
        }
    });
});

module.exports = router;









