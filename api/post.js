const express = require("express");
const connection = require("../database");

const router = express.Router();

router.get('/', (req, res) => {
    // Retrieve all posts from the database
        connection.query('SELECT * FROM post', (err, results) => {
        if (err) {
            res.status(500).json({ error: 'Internal Server Error' });
        } else {
            res.status(201).json(results);
        }
    });
});

router.post('/', (req, res) => {
    const { Title, Description, CreatedByID, CreatedByName} = req.body;

    // Insert a new post into the database
    connection.query('INSERT INTO post (Title, Description, CreatedByID, CreatedByName) VALUES (?, ?, ?, ?)', [Title, Description, CreatedByID, CreatedByName], (err, result) => {
        if (err) {
            res.status(500).json({ error: 'Internal Server Error' });
        } else {
            res.status(201).json({ message: 'Post created successfully' });
        }
    });
});


router.get('/:id', async (req, res) => {
    const id = req.params.id;
  
    // Insert the user into the database
    connection.query(
      'SELECT * FROM post WHERE CreatedByID = ?', [id],
      (err, results) => {
        if (err) {
          console.error('Error fetching post:', err);
          res.status(500).json({ error: 'Error fetching post' });
        }  
        
        if (results.length === 0) {
          res.status(404).json({ error: 'post not found' });
        } else {
            res.status(201).json(results);
        }
      }
    );
  });

  router.patch('/update/:id', (req, res) => {
    const PostID = req.params.id;
    const { Title, Description} = req.body;

    connection.query('UPDATE post SET Title = ?, Description = ?  WHERE PostID = ?', [Title, Description, PostID], (err, result) => {
        if (err) {
            res.status(500).json({ error: 'Internal Server Error' });
        } else if (result.affectedRows === 0) {
            res.status(404).json({ error: 'Post not found' });
        } else {
            res.status(201).json({ message: 'Post updated successfully' });
        }
    });
});

router.delete('/delete/:id', (req, res) => {
    const PostID = req.params.id;

    connection.query('DELETE FROM post WHERE PostID = ?', [PostID], (err, result) => {
        if (err) {
            res.status(500).json({ error: 'Internal Server Error' });
        } else if (result.affectedRows === 0) {
            res.status(404).json({ error: 'Post not found' });
        } else {
            res.status(201).json({ message: 'Post deleted successfully' });
        }
    });
});






module.exports = router;