const express = require("express");
const connection = require("../database");
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const multer = require('multer');


const router = express.Router();


const storage = multer.diskStorage({
  destination: './uploads',
  filename: function (req, file, cb) {
    cb(null, Date.now() + '-' + file.originalname);
  },
});
const upload = multer({ storage: storage });


router.post('/register', async (req, res) => {
  const { Email, Password, Username } = req.body;

  try {
    // Hash the password before storing it in the database
    const hashedPassword = await bcrypt.hash(Password, 10);

    // Insert the user into the database
    connection.query(
      'INSERT INTO user (Username, Email, Password) VALUES (?, ?, ?)',
      [Username, Email, hashedPassword],
      (err, results) => {
        if (err) {
          console.error('Error registering user:', err);
          res.status(500).json({ error: 'Registration failed' });
        } else {
          res.status(201).json({ message: 'Registration successful' });
        }
      }
    );
  } catch (error) {
    console.error('Error hashing password:', error);
    res.status(500).json({ error: 'Registration failed' });
  }
});


router.post('/login', (req, res) => {
  const { Email, Password } = req.body;
  console.log(Email);
  console.log(Password);


  // Check if the user exists in the database
  connection.query(
    'SELECT * FROM user WHERE Email = ?',
    [Email],
    async (err, results) => {
      if (err) {
        console.error('Error logging in:', err);
        res.status(500).json({ error: 'Login failed' });
      } else if (results.length === 0) {
        res.status(401).json({ error: 'Invalid credentials' });
      } else {
        const user = results[0];
        console.log(user.Password);


        const passwordMatch = await bcrypt.compare(Password, user.Password);

        console.log(passwordMatch);

        if (passwordMatch) {
          // Generate a JWT token for the authenticated user
          const token = jwt.sign(
            { UserID: user.UserID, Email: user.Email },
            'qwerty',
            { expiresIn: '1h' } // Token expiration time
          );

          res.status(201).json({ token });
        } else {
          res.status(401).json({ error: 'Invalid credentials' });
        }
      }
    }
  );
});

router.get('/', async (req, res) => {


  // Insert the user into the database
  connection.query(
    'SELECT * FROM user',
    (err, results) => {
      if (err) {
        console.error('Error fetching users:', err);
        res.status(500).json({ error: 'Error fetching users' });
      } else {
        res.status(201).json({ message: results });
      }
    }
  );


});

router.get('/:id', async (req, res) => {
  const id = req.params.id;

  // Insert the user into the database
  connection.query(
    'SELECT * FROM user WHERE UserID = ?', [id],
    (err, results) => {
      if (err) {
        console.error('Error fetching user:', err);
        res.status(500).json({ error: 'Error fetching user' });
      }  
      
      if (results.length === 0) {
        res.status(404).json({ error: 'User not found' });
      } else {
        const user = results[0];
        res.status(201).json({ user });
     
      }
    }
  );


});



router.patch('/update/:id', async (req, res) => {
  const { Email, Password, Username, PhotoUrl, } = req.body;
  const id = req.params.id;
  const hashedPassword = await bcrypt.hash(Password, 10);


  console.log(id);
  console.log(req.body);
  connection.query(
    'UPDATE user set Email = ?, Password = ?, Username = ?, PhotoUrl = ? WHERE UserID = ? ', [Email, hashedPassword,Username, PhotoUrl, id],
    (err, results) => {
     if(!err){
        if(results.affectedRows == 0){
          return res.status(404).json({message : "user id not found"})
        }
      return res.status(201).json({message : "User updated succesfully"})

     }else{
      return res.status(500).json({message : "Error Updating User"})

     }
    }
  );
});


router.delete('/delete/:id', (req, res) => {
  const id = req.params.id;

  
  connection.query('DELETE FROM user WHERE UserID = ?', [id], (err, results) => {
    if (err) {
      console.error('Error deleting user:', err);
      res.status(500).json({ error: 'Failed to delete user' });
      return;
    }

    if (results.affectedRows === 0) {
      res.status(404).json({ error: 'User not found' });
    } else {
      res.json({ message: 'User deleted successfully' });
    }
  });
});

router.post('/change-profile-photo', upload.single('photo'), async (req, res) => {
  try {
   
    const UserID = req.body.UserID;

    
    const photoFilename = req.file.filename;
    
   
    const query = 'UPDATE users SET profile_photo = ? WHERE id = ?';
    const [results] = await connection.execute(query, [photoFilename, UserID]);
    connection.release();

    res.status(201).json({ message: 'Profile photo updated' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal server error' });
  }
});


















module.exports = router;