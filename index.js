const express =  require("express");
const app = express()
const bodyParser = require('body-parser');
require("dotenv").config();
const cors = require('cors');



app.use(cors());
app.use(express.json());
app.use(bodyParser.urlencoded({extended: true}));


//routes
const userRoute = require("./api/user");
const postRoute = require("./api/post");
const commentRoute =require("./api/comment");
const notificationRoute = require("./api/notification");
const chatRoute = require("./api/chat");


app.use("/api/users", userRoute);
app.use("/api/posts", postRoute);
app.use("/api/comments", commentRoute);
app.use("/api/notifications", notificationRoute);
app.use("/api/chats", chatRoute);



app.listen(process.env.PORT, (req, res) =>{
    console.log("Server is running... ");

})
